@extends('layouts.base')

@section('page.title', $blog -> title)

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('blogs.update', $blog -> id) }}" method="POST">
            @method('PATCH')
            <x-form.form-header>Update blog</x-form.form-header>
            <x-form.form-input 
                type="text" 
                name="title" 
                placeholder="Type title of blog" 
                title="Title" 
                value="{{ $blog -> title }}"
            />

            <x-form.form-textarea 
                rows=10
                name="content" 
                placeholder="Type content" 
                title="Content" 
                value="{{ $blog -> content }}"
            />

            <div>
                @if ($categories->isEmpty())
                    <p class='text-center'>No categories to choose</p>
                @else
                    @foreach($categories as $category)
                        <input 
                            type="checkbox" 
                            id="{{ $category->title }}" 
                            name="categories[]" 
                            value="{{ $category->id }}" 
                            {{ $blog->categories->contains($category->id) ? 'checked' : '' }}
                        />
                        <label for="{{ $category->title }}">{{ $category->title }}</label>
                    @endforeach
                @endif
            </div>
            
            <x-form.form-button>Update</x-form.form-button>
        </x-form.form>
    </section>

@endsection