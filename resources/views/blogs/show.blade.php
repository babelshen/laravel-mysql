@extends('layouts.base')

@section('page.title', $blog -> title)

@section('content')
    <section>
        <x-pages.page-title>
            {{ $blog -> title }}
        
            <x-slot name='link'>
                <a href="{{ route('blogs.index') }}" >
                    ← Go back
                </a>
            </x-slot>

            @can('update', $blog)
                <x-slot name='button'>
                    <a href="{{ route('blogs.edit', $blog -> id) }}" >
                        Update
                    </a>
                </x-slot>
            @endcan
        </x-pages.page-title>
        <div class="flex items-end gap-4">
            <h2>Category:</h2>
            <x-pages.blogs-categories :blog="$blog" />
        </div>
        <div class="flex gap-1 my-4">
            <h2>Author:</h2>
            <a href="{{ route('users.show', $blog->user->id) }}">{{ $blog->user->name }}</a>
        </div>
        <x-pages.blogs-separator />
        <div class='my-6'>
            {!! $blog -> content !!}
        </div>
    </section>
@endsection