@extends('layouts.base')

@section('page.title', 'Create blog')

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('blogs.store') }}" method="POST">
            <x-form.form-header>Create blog</x-form.form-header>
            <x-form.form-input 
                type="text" 
                name="title" 
                placeholder="Type title of blog" 
                title="Title" 
                value="{{ old('title') }}"
            />

            <x-form.form-textarea 
                rows=10
                name="content" 
                placeholder="Type content" 
                title="Content" 
                value="{{ old('content') }}"
            />

            <div>
                @if ($categories->isEmpty())
                    <p class='text-center'>No categories to choose</p>
                @else
                    @foreach($categories as $category)
                        <input type="checkbox" id="{{ $category->title }}" name="categories[]" value="{{ $category->id }}" />
                        <label for="{{ $category->title }}">{{ $category->title }}</label>
                    @endforeach
                @endif
            </div>
            
            <x-form.form-button>Create</x-form.form-button>
        </x-form.form>
    </section>

@endsection