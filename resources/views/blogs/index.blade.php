@extends('layouts.base')

@section('page.title', 'Blog Page')

@section('content')
    <section>
        <x-pages.page-title>
            Interesting blogs
        
            <x-slot name='button'>
                <a href="{{ route("blogs.create") }}">
                    Create blog
                </a>
            </x-slot>
        
        </x-pages.page-title>

        <x-form.form action="{{ route('blogs.index') }}" method="GET">
        
            <x-form.form-input 
                type="text" 
                name="search" 
                placeholder="Search blog" 
                title="Search" 
                value="{{ old('search') }}"
            />

        </x-form.form>

        @if ($blogs->isEmpty())
            <p class='text-center'>No blogs found</p>
        @else
            <x-pages.blogs-wrapper>
                @foreach($blogs as $blog)
                    <x-pages.blogs-article :blog="$blog" />
                @endforeach
            </x-pages.blogs-wrapper>

            {{ $blogs -> links() }}
        @endif
    </section>
@endsection

