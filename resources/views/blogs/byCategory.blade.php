@extends('layouts.base')

@section('page.title', 'Blogs by category: ' . $category->title)

@section('content')
    <section>
        <h1 class="text-2xl font-bold mb-4">Blogs with Category: {{ $category->title }}</h1>
        
        @if ($blogs->isEmpty())
            <p>No blogs found with the category "{{ $categoty->title }}"</p>
        @else
            <x-pages.blogs-wrapper>
                @foreach($blogs as $blog)
                    <x-pages.blogs-article :blog="$blog" />
                @endforeach
            </x-pages.blogs-wrapper>
        @endif
    </section>
@endsection
