@extends('layouts.base')

@section('page.title', $category->title)

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('categories.update', $category -> id) }}" method="POST">
            @method('PATCH')
            <x-form.form-header>Update category</x-form.form-header>
            <x-form.form-input 
                type="text" 
                name="title" 
                placeholder="Type title of the category" 
                title="Title" 
                value="{{ $category->title }}"
            />

            <x-form.form-input 
                type="text" 
                name="color" 
                placeholder="Type color property for the category" 
                title="Color" 
                value="{{ $category->color }}"
            />

            <x-form.form-input 
                type="text" 
                name="background" 
                placeholder="Type background property for the category" 
                title="Background" 
                value="{{ $category->background }}"
            />
            
            <x-form.form-button>Update</x-form.form-button>
        </x-form.form>
    </section>

@endsection