@extends('layouts.base')

@section('page.title', 'Admin Panel')

@section('content')
    <section>
        <x-pages.page-title>
            List of categories
        
            <x-slot name='button'>
                <a href="{{ route("categories.create") }}">
                    Create category
                </a>
            </x-slot>
        
        </x-pages.page-title>

        @if ($categories->isEmpty())
            <p class='text-center'>No categories yet</p>
        @else
            <x-pages.blogs-wrapper>
                @foreach($categories as $category)
                    <div class="{{ $category->color }} {{ $category->background }} py-1 px-3 rounded-lg flex justify-between items-center">
                        <span>{{ $category->title }}</span>
                        <div class="flex items-center gap-2">
                            <form action="{{ route('categories.edit', $category -> id) }}" method='GET' class="px-1 py-1 bg-white border-2 rounded-full border-black">
                                @csrf
                                <button type='submit' class="text-black">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="text-black" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0 3.181 3.183a8.25 8.25 0 0 0 13.803-3.7M4.031 9.865a8.25 8.25 0 0 1 13.803-3.7l3.181 3.182m0-4.991v4.99" />
                                    </svg>                                      
                                </button>
                            </form>
                            <form action="{{ route('categories.destroy', $category -> id) }}" method='POST' class="px-1 py-1 bg-white border-2 rounded-full border-black">
                                @method('DELETE')
                                @csrf
                                <button type='submit' class="text-black">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="text-black" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                                    </svg>                                      
                                </button>
                            </form>
                        </div>
                    </div>
                @endforeach
            </x-pages.blogs-wrapper>
        @endif
    </section>
@endsection

