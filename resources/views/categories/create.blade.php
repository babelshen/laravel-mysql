@extends('layouts.base')

@section('page.title', 'Create category')

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('categories.store') }}" method="POST">
            <x-form.form-header>Create category</x-form.form-header>
            <x-form.form-input 
                type="text" 
                name="title" 
                placeholder="Type title of the category" 
                title="Title" 
                value="{{ old('title') }}"
            />

            <x-form.form-input 
                type="text" 
                name="color" 
                placeholder="Type color property for the category" 
                title="Color" 
                value="{{ old('color') }}"
            />

            <x-form.form-input 
                type="text" 
                name="background" 
                placeholder="Type background property for the category" 
                title="Background" 
                value="{{ old('background') }}"
            />
            
            <x-form.form-button>Create</x-form.form-button>
        </x-form.form>
    </section>

@endsection