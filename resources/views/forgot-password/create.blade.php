@extends('layouts.base')

@section('page.title', 'Reset Password Page')

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('forgot-password.store') }}" method="POST">
            <x-form.form-header>Reset password</x-form.form-header>
            <x-form.form-input 
                type="email" 
                name="email" 
                placeholder="Type your e-mail" 
                title="E-mail" 
                value="{{ old('email') }}"
            />

            <x-form.form-button>Reset</x-form.form-button>
        </x-form.form>
    </section>

@endsection