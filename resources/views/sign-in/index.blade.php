@extends('layouts.base')

@section('page.title', 'Login Page')

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('auth.sign-in.store') }}" method="POST">
            <x-form.form-header>Sign in</x-form.form-header>
            <x-form.form-input 
                type="email" 
                name="email" 
                placeholder="Type your e-mail" 
                title="E-mail" 
                value="{{ old('email') }}"
            />
    
            <x-form.form-input 
                type="password" 
                name="password" 
                placeholder="Type your password" 
                title="Password" 
                value="{{ old('password') }}"
            />

            <div class="flex items-center justify-between mb-6">
                <div class="flex items-center gap-2">
                    <input type="checkbox" id="remember" name="remember" class="h-4 w-4 rounded border-gray-300 text-green-600 focus:ring-green-500" />
                    <label for="remember" class="text-sm text-gray-900">Remember me</label>
                </div>
                <a href="{{ route('forgot-password.create') }}" class="text-sm font-medium hover:text-blue-500">Forgot your password?</a>
            </div>
            <x-form.form-button>Sign In</x-form.form-button>
        </x-form.form>
    </section>

@endsection