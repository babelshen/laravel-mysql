@extends('layouts.base')

@section('page.title', $user->name)

@section('content')
    <h1 class='text-center text-2xl font-semibold'>{{ $user->name }}</h1>
    <div class="my-8 flex justify-center gap-6 items-center">
        <div class="max-w-28 max-h-28 rounded-full">
            <img class="w-full h-full rounded-full" src="{{ $user->avatar }}" alt="User Avatar" title="{{ $user->name }}" />
        </div>
        <div class="flex flex-col gap-2">
            <p>User Name: {{ $user->name }}</p>
            <p>Email: {{ $user->email }}</p>
            @can('update', $user)
            <a 
                class='text-center py-2 px-4 block rounded-md border-0 text-black ring-1 ring-inset ring-gray-300 bg-gray-800 text-white transition ease-in-out duration-300 placeholder:text-gray-400 focus:bg-gray-700 hover:bg-gray-700 sm:text-sm sm:leading-6'
                href="{{ route('users.edit', $user->id) }}"
            >
                Edit profile
            </a>
            @endcan
        </div>
    </div>
    
    <h2 class='text-center text-2xl font-semibold'>User's Blogs</h2>
    @if ($blogs->isEmpty())
        <p>The user does not have any blogs yet</p>
    @else
        <x-pages.blogs-wrapper>
            @foreach($blogs as $blog)
                <x-pages.blogs-article :blog="$blog" />
            @endforeach
        </x-pages.blogs-wrapper>
    @endif
    {{ $blogs->links() }}
@endsection
