@extends('layouts.base')

@section('page.title', 'Change profile')

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('users.update', $user->id) }}" method="POST">
            @method('PATCH')
            <x-form.form-header>Update blog</x-form.form-header>
            <x-form.form-input 
                type="url" 
                name="url" 
                placeholder="Type title of blog" 
                title="Link to image" 
                value="{{ $user->avatar }}"
            />

            <x-form.form-input 
                type="text" 
                name="name" 
                placeholder="Type new name" 
                title="New name" 
                value="{{ $user->name }}"
            />

            <x-form.form-button>Update</x-form.form-button>
        </x-form.form>
    </section>

@endsection