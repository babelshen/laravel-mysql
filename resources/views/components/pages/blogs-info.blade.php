@props([
    'avatar' => '',
    'userName' => '',
    'userId' => 0,
    'date' => '',
])

<div class='flex items-center justify-evenly'>
    <div class="max-w-10 max-h-10">
        @if ($avatar)
            <img src="{{ $avatar }}" alt="User Avatar" class="w-full h-full rounded-full mr-2">
        @else
            <svg class="w-10 h-10 rounded-full bg-gray-200"></svg>
        @endif
    </div>
    <div class='flex flex-col'>
        <span>
            <p>Published by:</p>
            <a class='hover:text-slate-600' href="{{ route('users.show', $userId) }}">{{ $userName }}</a>
        </span>
        <div class='text-slate-500'>{{ $date }}</div>
    </div>
</div>