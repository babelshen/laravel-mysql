<div class='flex justify-start gap-11 items-center'>

    @isset($link)

        {{ $link }}

    @endisset

    <h1 class='font-bold text-lg text-center'>
        {{ $slot }}    
    </h1>

    @isset($button)

        {{ $button }}
    
    @endisset
</div>