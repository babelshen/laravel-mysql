<article class='text-center my-6 border-2 border-stone-200 border-solid px-4 py-4 rounded-xl'>
    <x-pages.blogs-title href="{{ route('blogs.show', $blog->id) }}" :blog="$blog" />                      
    <x-pages.blogs-content :blog="$blog" />
    <x-pages.blogs-separator />
    <x-pages.blogs-info 
        userId="{{ $blog->user->id }}"
        userName="{{ $blog->user->name }}"
        date="{{ $blog->published_at->diffForHumans() }}"
        avatar="{{ $blog->user->avatar }}"
    />
    <x-pages.blogs-categories :blog="$blog" />

    @can('delete', $blog)
        <x-form.form action="{{ route('blogs.destroy', $blog->id) }}" method='POST'>
            @method('DELETE')
            <x-form.form-button>Delete</x-form.form-button>
        </x-form.form>
    @endcan
</article>
