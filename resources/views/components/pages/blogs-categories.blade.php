<div class="mt-4">
    @foreach ($blog->categories as $category)
        <a 
            href="{{ route('blogs.byCategory', $category) }}" 
            class="{{ $category->color }} {{ $category->background }} rounded-full px-3 py-1 text-sm font-semibold mr-2"
        >
            {{ $category->title }}
        </a>
    @endforeach
</div>