@props([
    'href' => '',
    'blog' => null,
])
<h2 class='font-semibold mb-2'>
    <a href="{{ $href }}" >
        {{ (strlen($blog->title) > 20 ? substr($blog->title, 0, 20) . '...' : $blog->title) }}
    </a>  
</h2>