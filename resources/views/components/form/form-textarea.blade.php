@props([
    'name' => 'content',
    'title' => 'Text',
    'placeholder' => 'Placeholder',
    'value' => '',
    'rows' => 10,
])

<div class='flex flex-col gap-1'>
    <label for="{{ $name }}" class="block text-sm font-medium leading-6 text-gray-900"> {{ $title }} </label>
    <div class="relative rounded-md shadow-sm">
        <textarea 
            name="{{ $name }}"
            id="{{ $name }}"
            class="resize-none block w-full rounded-md border-0 py-1.5 pl-4 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" 
            placeholder="{{ $placeholder }}"
            rows="{{ $rows }}"
        >{{ $value }}</textarea>
    </div>
    <x-form.form-error name="{{ $name }}" />
</div>