<form 
    {{ $attributes }}
    class="flex flex-col gap-4 max-w-full m-auto border-2 border-stone-200 border-solid px-6 py-6 my-10 rounded-xl"
>
    @csrf
    {{ $slot }}
</form>
