<button 
    class='py-2 px-4 block rounded-md border-0 text-black ring-1 ring-inset ring-gray-300 bg-gray-800 text-white transition ease-in-out duration-300 placeholder:text-gray-400 focus:bg-gray-700 hover:bg-gray-700 sm:text-sm sm:leading-6'
    {{ $attributes 
        -> merge([
            'type' => 'submit',
        ]); 
    }}
    >
{{ $slot }}
</button>