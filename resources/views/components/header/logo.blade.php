<div class="flex flex-shrink-0 items-center">
    <a class='text-white' href="{{ route('home.index') }}">
        {{ config('app.name') }}
    </a>
</div>