<?php

namespace App\Policies;

use App\Models\Blog;
use App\Models\User;

class BlogPolicy
{
    public function before(User $user): ?bool
    {
        if ($user->admin) {
            return true;
        }

        return null;
    }

    public function viewAny(): bool
    {
        return true;
    }

    public function view(): bool
    {
        return true;
    }

    public function create(): bool
    {
        return true;
    }

    public function update(User $user, Blog $blog): bool
    {
        return $user->id === $blog->user_id;
    }

    public function delete(User $user, Blog $blog): bool
    {
        return $user->id === $blog->user_id;
    }
}
