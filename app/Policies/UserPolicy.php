<?php

namespace App\Policies;

use App\Models\User;

class UserPolicy
{
    public function before(User $user): ?bool
    {
        if ($user->admin) {
            return true;
        }

        return null;
    }

    public function viewAny(): bool
    {
        return true;
    }

    public function view(): bool
    {
        return true;
    }

    public function create(): bool
    {
        return true;
    }

    public function update(User $authenticatedUser, User $profileUser): bool
    {
        return $authenticatedUser->id === $profileUser->id;
    }

    public function delete(): bool
    {
        return false;
    }
}
