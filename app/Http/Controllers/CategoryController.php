<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::getAllCategories();

        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => ['required', 'string', 'min: 3', 'max: 15'],
            'color' => ['required', 'string', 'min: 4', 'max: 20'],
            'background' => ['required', 'string', 'min: 4', 'max: 20'],
        ]);

        if (! $validated) {
            return redirect()->back()->withInput();
        } else {

            $categories = Category::createCategory($validated);

            return view('categories.index', compact('categories'));
        }
    }

    public function edit($categoryId)
    {
        $category = Category::getCategoryById($categoryId);

        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, $categoryId)
    {
        $validated = $request->validate([
            'title' => ['nullable', 'string', 'min: 3', 'max: 15'],
            'color' => ['nullable', 'string', 'min: 4', 'max: 20'],
            'background' => ['nullable', 'string', 'min: 4', 'max: 20'],
        ]);

        $categories = Category::updateCategory($validated, $categoryId);

        return view('categories.index', compact('categories'));
    }

    public function destroy($categoryId)
    {
        $categories = Category::deleteCategory($categoryId);

        return view('categories.index', compact('categories'));
    }
}
