<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function index()
    {
        return View::make('sign-in.index');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => ['required', 'string', 'email'],
            'password' => ['required'],
        ]);

        if (! $validated) {
            return redirect()->back()->withInput();
        }

        if (Auth::attempt($validated, $request->boolean('remember'))) {
            $request->session()->regenerate();

            return redirect()->route('blogs.index');
        } else {
            throw ValidationException::withMessages([
                'password' => 'E-mail or password is not correct',
            ]);
        }
    }
}
