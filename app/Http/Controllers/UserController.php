<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function show($userId)
    {
        $user = User::getUserById($userId);
        $blogs = $user->blogs()->paginate(6);

        return view('users.show', compact('user', 'blogs'));
    }

    public function edit($userId)
    {
        $user = User::getUserById($userId);
        Gate::authorize('update', $user);

        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $userId)
    {
        $validated = $request->validate([
            'url' => ['nullable', 'string', 'url', 'min:5', 'max:100'],
            'name' => ['nullable', 'string', 'min:3', 'max:20'],
        ]);

        $user = User::updateUser($validated, $userId);

        $blogs = $user->blogs()->paginate(6);

        return view('users.show', compact('user', 'blogs'));
    }
}
