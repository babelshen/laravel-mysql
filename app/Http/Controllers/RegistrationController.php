<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Password;

class RegistrationController extends Controller
{
    public function index()
    {
        return view('sign-up.index');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => ['required', 'string', 'email', 'min: 6', 'max: 40', 'unique:users'],
            'password' => ['required', Password::min(6)->max(20)->letters()->mixedCase()->numbers()->symbols(), 'confirmed'],
            'password_confirmation' => ['required', Password::min(6)->max(20)->letters()->mixedCase()->numbers()->symbols()],
        ]);

        if (! $validated) {
            return redirect()->back()->withInput();
        }

        $user = new User;
        $user->name = $validated['email'];
        $user->email = $validated['email'];
        $user->password = bcrypt($validated['password']);
        $user->save();

        Auth::login($user);

        return redirect()->route('blogs.index');
    }
}
