<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $validated = $request->validate([
            'search' => ['nullable', 'string', 'min: 4', 'max: 20'],
            'page' => ['nullable', 'integer', 'min: 1', 'max: 20'],
        ]);

        $searchBlog = $validated['search'] ?? '';

        $blogs = Blog::getAllBlogs($searchBlog);

        return view('blogs.index', compact('blogs'));
    }

    public function show($blogId)
    {
        $blog = Blog::getBlogById($blogId);

        return view('blogs.show', compact('blog'));
    }

    public function create()
    {
        $categories = Category::getAllCategories();

        return view('blogs.create', compact('categories'));
    }

    public function blogsByCategory($categoryId)
    {
        $category = Category::getCategoryById($categoryId);
        $blogs = $category->blogs;

        return view('blogs.byCategory', compact('category', 'blogs'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => ['required', 'string', 'min: 5', 'max: 100'],
            'content' => ['required', 'string', 'min: 10', 'max:500'],
            'categories' => ['nullable', 'array'],
        ]);

        if (! $validated) {
            return redirect()->back()->withInput();
        } else {
            Blog::createBlog($validated);

            return redirect()->route('blogs.index');
        }
    }

    public function edit($blogId)
    {
        $blog = Blog::getBlogById($blogId);
        Gate::authorize('update', $blog);
        $categories = Category::getAllCategories();

        return view('blogs.edit', compact('blog', 'categories'));
    }

    public function update(Request $request, $blogId)
    {
        $validated = $request->validate([
            'title' => ['nullable', 'string', 'min:5', 'max:100'],
            'content' => ['nullable', 'string', 'min:10', 'max:500'],
            'categories' => ['nullable', 'array'],
        ]);

        $blog = Blog::updateBlog($validated, $blogId);

        return view('blogs.show', compact('blog'));
    }

    public function destroy($blogId)
    {
        $blog = Blog::getBlogById($blogId);
        Gate::authorize('delete', $blog);
        $blogs = Blog::deleteBlog($blog);

        return view('blogs.index', compact('blogs'));
    }
}
