<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class LogOutController extends Controller
{
    public function index()
    {
        Auth::logout();

        return redirect()->route('auth.sign-in.index');
    }
}
