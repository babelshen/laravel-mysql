<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $title
 * @property string $color
 * @property string $background
 */
class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'color',
        'background',
    ];

    public static function getAllCategories()
    {
        return static::get();
    }

    public static function getCategoryById($categoryId)
    {
        return static::findOrFail($categoryId);
    }

    public static function createCategory($validated)
    {
        static::create([
            'title' => $validated['title'],
            'color' => $validated['color'],
            'background' => $validated['background'],
        ]);

        return static::getAllCategories();
    }

    public static function updateCategory($validated, $categoryId)
    {
        $category = Category::findOrFail($categoryId);
        $category->update([
            'title' => $validated['title'] ?? $category->title,
            'color' => $validated['color'] ?? $category->color,
            'background' => $validated['background'] ?? $category->background,
        ]);

        return static::getAllCategories();
    }

    public static function deleteCategory($categoryId)
    {
        $category = static::getCategoryById($categoryId);
        $category->delete();

        return static::getAllCategories();
    }

    public function blogs()
    {
        return $this->belongsToMany(Blog::class, 'blog_categories', 'category_id', 'blog_id');
    }
}
