<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $user_id
 * @property Carbon $published_at
 * @property string $title
 * @property text $content
 */
class Blog extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'published_at',
        'title',
        'content',
    ];

    protected function casts(): array
    {
        return [
            'published_at' => 'date',
        ];
    }

    public static function getAllBlogs($search, $perPage = 6): LengthAwarePaginator
    {
        return static::query()
            ->where('title', 'like', "%{$search}%")
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    public static function getBlogById($blogId)
    {
        return static::query()
            ->findOrFail($blogId);
    }

    public static function createBlog($validated)
    {
        $blog = Blog::query()->create([
            'user_id' => auth()->id(),
            'title' => $validated['title'],
            'content' => $validated['content'],
            'published_at' => new Carbon($validated['published_at'] ?? null),
        ]);

        if (isset($validated['categories'])) {
            $blog->categories()->attach($validated['categories']);
        }

        return $blog;
    }

    public static function updateBlog($validated, $blogId)
    {
        $blog = static::getBlogById($blogId);
        $blog->update([
            'title' => $validated['title'] ?? $blog->title,
            'content' => $validated['content'] ?? $blog->content,
        ]);

        if (isset($validated['categories'])) {
            $blog->categories()->sync($validated['categories']);
        } else {
            $blog->categories()->detach();
        }

        return static::getBlogById($blogId);
    }

    public static function deleteBlog($blog)
    {
        $blog->delete();

        return Blog::orderBy('created_at', 'desc')->paginate(6);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'blog_categories', 'blog_id', 'category_id');
    }
}
