<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property string $name
 * @property string $email
 * @property string $avatar
 * @property string $password
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $attributes = [
        'admin' => false,
    ];

    protected $fillable = [
        'name',
        'email',
        'avatar',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected function casts(): array
    {
        return [
            'password' => 'hashed',
        ];
    }

    public static function getUserById($userId)
    {
        return static::findOrFail($userId);
    }

    public static function updateUser($validated, $userId)
    {
        $user = static::getUserById($userId);
        $user->update([
            'name' => $validated['name'] ?? $user->name,
            'avatar' => $validated['url'] ?? $user->avatar,
        ]);

        return static::findOrFail($userId);
    }

    public function blogs(): HasMany
    {
        return $this->hasMany(Blog::class);
    }
}
