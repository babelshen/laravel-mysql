<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('blog_categories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('blog_id')->comment('Id of the blog');
            $table->unsignedBigInteger('category_id')->comment('Id of the category');

            $table->index('blog_id', 'blog_category_blog_idx');
            $table->index('category_id', 'blog_category_category_idx');

            $table->foreign('blog_id', 'blog_category_blog_fk')
                ->on('blogs')
                ->references('id')
                ->cascadeOnDelete()
                ->comment('Foreign key referencing the blogs table');

            $table->foreign('category_id', 'blog_category_category_fk')
                ->on('categories')
                ->references('id')
                ->cascadeOnDelete()
                ->comment('Foreign key referencing the categories table');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('blog_categories');
    }
};
