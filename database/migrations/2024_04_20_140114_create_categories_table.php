<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('title')->default('None')->comment('Name of the category');
            $table->string('background')->default('bg-black')->comment('Background color of the category badge');
            $table->string('color')->default('text-white')->comment('Color of the category badge');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
