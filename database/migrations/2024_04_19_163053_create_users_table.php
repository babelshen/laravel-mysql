<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id()
                ->from(100)
                ->comment('User id');

            $table->timestamps();

            $table->string('name')
                ->unique()
                ->comment('Name of user');

            $table->string('avatar')
                ->nullable()
                ->default('https://i.servimg.com/u/f21/18/21/41/30/na1110.png')
                ->comment('Link to avatar');

            $table->string('email')
                ->unique()
                ->comment('E-mail address. Must be unique');

            $table->string('password')
                ->comment('Password of the user');

            $table->rememberToken();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
