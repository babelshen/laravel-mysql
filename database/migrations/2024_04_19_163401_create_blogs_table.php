<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('user_id')
                ->constrained()
                ->cascadeOnDelete()
                ->index()
                ->comment('Connection with Users table');

            $table->timestamp('published_at')
                ->nullable()
                ->comment('Date of blog published');

            $table->string('title')->index();

            $table->text('content');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('blogs');
    }
};
