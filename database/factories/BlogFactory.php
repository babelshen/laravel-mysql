<?php

namespace Database\Factories;

use App\Models\Blog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    protected $model = Blog::class;

    public function definition()
    {
        $userIds = User::pluck('id')->toArray();
        $userId = $this->faker->randomElement($userIds);

        return [
            'user_id' => $userId,
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'published_at' => Carbon::now(),
        ];
    }
}
