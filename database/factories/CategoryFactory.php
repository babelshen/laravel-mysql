<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    protected $model = Category::class;

    public function definition()
    {
        $colors = ['bg-red-500', 'bg-blue-500', 'bg-green-500', 'bg-yellow-500', 'bg-purple-500'];
        $backgrounds = ['text-white', 'text-black'];

        return [
            'title' => $this->faker->word,
            'color' => $this->faker->randomElement($colors),
            'background' => $this->faker->randomElement($backgrounds),
        ];
    }
}
