<?php

namespace Database\Seeders;

use App\Models\Blog;
use Illuminate\Database\Seeder;

class DatabaseBlogSeeder extends Seeder
{
    public function run(): void
    {
        Blog::factory(10)->create();
    }
}
