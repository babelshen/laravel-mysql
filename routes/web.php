<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogOutController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

Route::view('/', 'home.index')->name('home.index');
Route::redirect('/main', '/', 301);

Route::prefix('/blogs')->middleware('auth')->group(function () {
    Route::get('/', [BlogController::class, 'index'])->name('blogs.index');
    Route::get('/{blog}', [BlogController::class, 'show'])->name('blogs.show')->whereNumber('blog');
    Route::get('/categories/{category}', [BlogController::class, 'blogsByCategory'])->name('blogs.byCategory')->whereNumber('category');
    Route::get('/create', [BlogController::class, 'create'])->name('blogs.create');
    Route::post('/', [BlogController::class, 'store'])->name('blogs.store');
    Route::get('/{blog}/edit', [BlogController::class, 'edit'])->name('blogs.edit')->whereNumber('blog');
    Route::patch('/{blog}', [BlogController::class, 'update'])->name('blogs.update')->whereNumber('blog');
    Route::delete('/{blog}', [BlogController::class, 'destroy'])->name('blogs.destroy')->whereNumber('blog');
});

Route::resource('/sign-up', RegistrationController::class)->only([
    'index', 'store',
])->middleware('guest');

Route::prefix('/auth')->as('auth.')->group(function () {
    Route::redirect('/', '/auth/sign-in', 301);
    Route::get('/sign-in', [LoginController::class, 'index'])->name('sign-in.index')->middleware('guest');
    Route::post('/sign-in', [LoginController::class, 'store'])->name('sign-in.store')->middleware('guest');
});

Route::get('/sign-out', [LogOutController::class, 'index'])->name('sign-out.index')->middleware('auth');

Route::get('/forgot-password', [ForgotPasswordController::class, 'create'])->name('forgot-password.create')->middleware('guest');
Route::post('/forgot-password', [ForgotPasswordController::class, 'store'])->name('forgot-password.store')->middleware('guest');
Route::get('/reset-password', [ResetPasswordController::class, 'create'])->name('password.reset')->middleware('guest');

Route::prefix('/categories')->middleware('auth')->group(function () {
    Route::get('/', [CategoryController::class, 'index'])->name('categories.index');
    Route::get('/create', [CategoryController::class, 'create'])->name('categories.create');
    Route::post('/', [CategoryController::class, 'store'])->name('categories.store');
    Route::get('/{category}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
    Route::patch('/{category}', [CategoryController::class, 'update'])->name('categories.update');
    Route::delete('/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');
});

Route::get('/users/{userId}', [UserController::class, 'show'])->name('users.show')->middleware('auth')->whereNumber('userId');
Route::get('/users/{userId}/edit', [UserController::class, 'edit'])->name('users.edit')->middleware('auth')->whereNumber('userId');
Route::patch('/{userId}', [UserController::class, 'update'])->name('users.update')->middleware('auth')->whereNumber('userId');

Route::fallback(function () {
    return Redirect::to('/');
}
);
